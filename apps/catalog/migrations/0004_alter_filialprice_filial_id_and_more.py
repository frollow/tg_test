# Generated by Django 4.1.4 on 2022-12-24 17:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("catalog", "0003_alter_filialprice_filial_id_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="filialprice",
            name="filial_id",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="filial_price",
                to="catalog.filial",
                verbose_name="Номер филиала",
            ),
        ),
        migrations.AlterField(
            model_name="filialprice",
            name="product_id",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="filial_price",
                to="catalog.product",
                verbose_name="Идентификатор продукта",
            ),
        ),
    ]
