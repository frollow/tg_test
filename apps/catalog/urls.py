from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ProductViewSet

app_name = "catalog"

router = DefaultRouter()
router.register("catalog", ProductViewSet, basename="catalog")


urlpatterns = [
    path("", include(router.urls)),
]
