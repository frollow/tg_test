from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=200, verbose_name="Название продукта")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"


class Filial(models.Model):
    name = models.CharField(max_length=200, verbose_name="Филиал")
    region = models.PositiveIntegerField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Регион",
        help_text="Идентификатор региона",
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name = "Филиал"
        verbose_name_plural = "Филиалы"


class Characterictic(models.Model):
    self = models.ForeignKey("self", on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=200, verbose_name="Филиал")
    product_id = models.ManyToManyField(
        Product,
        related_name="characterictic",
        blank=True,
        verbose_name="Характеристики",
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name = "Характеристики"
        verbose_name_plural = "Характеристики"


class FilialPrice(models.Model):
    product_id = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name="filial_price",
        null=True,
        verbose_name="Идентификатор продукта",
    )
    filial_id = models.ForeignKey(
        Filial,
        on_delete=models.CASCADE,
        related_name="filial_price",
        null=True,
        verbose_name="Номер филиала",
    )
    price = models.PositiveIntegerField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Цена",
        help_text="Стоимость товара",
    )

    def __str__(self):
        return self.product_id.name

    class Meta:
        ordering = ("pk",)
        verbose_name = "Цена в филиале"
        verbose_name_plural = "Цены в филиалах"
