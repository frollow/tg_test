from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import viewsets

from .models import Filial, FilialPrice, Characterictic, Product
from .serializers import (
    FilialSerializer,
    FilialPriceSerializer,
    CharactericticSerializer,
    ProductSerializer,
)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ("filial_price__filial_id",)
