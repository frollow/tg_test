from rest_framework import serializers

from .models import Filial, FilialPrice, Characterictic, Product


class FilialPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilialPrice
        fields = ("price",)


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        depth = 1
        fields = ("id", "name", "filial_price")


class FilialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Filial
        fields = ("id", "name", "region")


class CharactericticSerializer(serializers.ModelSerializer):
    class Meta:
        model = Characterictic
        fields = ("id", "name", "self", "product_id")


class FilialPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilialPrice
        fields = ("id", "product_id", "filial_id", "price")
