from django.contrib import admin

from .models import Filial, FilialPrice, Characterictic, Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)
    empty_value_display = "-пусто-"


admin.site.register(Product, ProductAdmin)


class FilialAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)
    empty_value_display = "-пусто-"


admin.site.register(Filial, FilialAdmin)


class CharactericticAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)
    empty_value_display = "-пусто-"


admin.site.register(Characterictic, CharactericticAdmin)


class FilialPriceAdmin(admin.ModelAdmin):
    list_display = ("product_id", "price")
    empty_value_display = "-пусто-"


admin.site.register(FilialPrice, FilialPriceAdmin)
