
**Создать файл .env в папке infra/tg_test/ и в него записать:**

DB_ENGINE=django.db.backends.postgresql

DB_NAME=postgres

POSTGRES_USER=postgres

POSTGRES_PASSWORD=postgres

DB_HOST=db

DB_PORT=5432

SECRET_KEY='django-insecure-oyud#&blo_txx&0ra4%9so&wcz6fqy4$=q3)izgn=yxj_rv%p0'

  
  

**Запустить команду из папки /tg_test/ :**

docker compose -f "infra/docker-compose.yaml" up -d --build

docker exec infra-web-1 python manage.py collectstatic --no-input

docker exec infra-web-1 python manage.py makemigrations

docker exec infra-web-1 python manage.py migrate

docker exec --tty infra-web-1 python manage.py createsuperuser --noinput --email admin@mail.ru --username admin

  

**Открыть страницу: http://127.0.0.1/**